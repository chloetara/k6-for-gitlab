'use strict'

import http from 'k6/http';
import { sleep, check } from 'k6';

let mainURL = 'https://inclusiva.io';

//  Entry 5
export let options = {
    vus: 50,
    duration: '10m'
};

// Entry 4
/* export let options = {
    vus: 40,
    duration: '5m'
}; */

// Entry 3 
/* export let options = {
    vus: 30,
    duration: '3m'
}; */

// Entry 2
/* export let options = {
    vus: 30,
    duration: '1m'
}; */

// Entry 1
/* export let options = {
    vus: 30,
    duration: '30s'
} */

export default function() {
    let resp = http.get(mainURL);

    check(resp, {
    'Response status is 200': (resp) => resp.status === 200,
    });

// for "sleep" time in seconds
    sleep(1);
};
