  execution: local
     script: check-resp1-ramp1.js
     output: -

  scenarios: (100.00%) 1 scenario, 50 max VUs, 38m30s max duration (incl. graceful stop):
           * default: Up to 50 looping VUs for 38m0s over 7 stages (gracefulRampDown: 30s, gracefulStop: 30s)

WARN[0124] Request Failed ------- error="context deadline exceeded"    
WARN[0185] Request Failed   ------- error="Get \"https://inclusiva.io\": context deadline exceeded"   
WARN[0246] Request Failed   ------- error="Get \"https://inclusiva.io\": context deadline exceeded"   
WARN[0307] Request Failed   ------- error="Get \"https://inclusiva.io\": context deadline exceeded"   
WARN[0368] Request Failed   ------- error="Get \"https://inclusiva.io\": context deadline exceeded"   
WARN[0429] Request Failed   ------- error="Get \"https://inclusiva.io\": context deadline exceeded"   
WARN[0490] Request Failed   ------- error="Get \"https://inclusiva.io\": context deadline exceeded"   
WARN[0551] Request Failed   ------- error="Get \"https://inclusiva.io\": context deadline exceeded"    
WARN[0612] Request Failed  ------- error="Get \"https://inclusiva.io\": context deadline exceeded"    
WARN[0673] Request Failed  ------- error="Get \"https://inclusiva.io\": context deadline exceeded"     
WARN[0734] Request Failed  ------- error="Get \"https://inclusiva.io\": context deadline exceeded"    
WARN[0795] Request Failed   ------- error="Get \"https://inclusiva.io\": context deadline exceeded"     
WARN[0856] Request Failed   ------- error="Get \"https://inclusiva.io\": context deadline exceeded"     
WARN[0917] Request Failed   ------- error="Get \"https://inclusiva.io\": context deadline exceeded"     
WARN[0978] Request Failed   ------- error="Get \"https://inclusiva.io\": context deadline exceeded"    
WARN[1010] Request Failed   ------- error="Get \"https://inclusiva.io\": read tcp 192.168.0.52:49804->35.227.24.163:443: read: connection timed out"    

WARN[1765] Request Failed   ------- error="context deadline exceeded"    
WARN[1787] Request Failed   ------- error="context deadline exceeded"    
WARN[1788] Request Failed   ------- error="context deadline exceeded"    
WARN[1801] Request Failed   ------- error="context deadline exceeded"    
WARN[1811] Request Failed   ------- error="context deadline exceeded"    
WARN[1814] Request Failed   ------- error="context deadline exceeded"    
WARN[1819] Request Failed   ------- error="context deadline exceeded"    
WARN[1865] Request Failed   ------- error="context deadline exceeded"    
WARN[1875] Request Failed   ------- error="context deadline exceeded"    
WARN[1894] Request Failed   ------- error="context deadline exceeded"    
WARN[1898] Request Failed   ------- error="context deadline exceeded"    
WARN[1905] Request Failed   ------- error="context deadline exceeded"    
WARN[1915] Request Failed   ------- error="context deadline exceeded"    
WARN[1935] Request Failed   ------- error="context deadline exceeded"    
WARN[1951] Request Failed   ------- error="context deadline exceeded"    
WARN[1994] Request Failed   ------- error="context deadline exceeded"    

running (38m02.6s), 00/50 VUs, 38366 complete and 0 interrupted iterations
default ✓ [======================================] 00/50 VUs  38m0s

     ✗ Response status is 200
      ↳  99% — ✓ 38334 / ✗ 32

     checks.........................: 99.91% ✓ 38334 ✗ 32   
     data_received..................: 2.9 GB 1.3 MB/s
     data_sent......................: 8.6 MB 3.8 kB/s
     http_req_blocked...............: avg=965.03µs min=206ns    med=596ns    max=1.5s  p(90)=857ns    p(95)=965ns   
     http_req_connecting............: avg=453.12µs min=0s       med=0s       max=1.18s p(90)=0s       p(95)=0s      
     http_req_duration..............: avg=708.37ms min=221.92ms med=595.49ms max=1m0s  p(90)=1.03s    p(95)=1.18s   
       { expected_response:true }...: avg=685.96ms min=221.92ms med=595.38ms max=1m0s  p(90)=1.03s    p(95)=1.17s   
     http_req_failed................: 0.03%  ✓ 15    ✗ 38351
     http_req_receiving.............: avg=365.49ms min=0s       med=258.83ms max=59.8s p(90)=637.14ms p(95)=798.65ms
     http_req_sending...............: avg=68.66µs  min=18.48µs  med=66.69µs  max=1.1ms p(90)=89.74µs  p(95)=99.08µs 
     http_req_tls_handshaking.......: avg=509µs    min=0s       med=0s       max=1.32s p(90)=0s       p(95)=0s      
     http_req_waiting...............: avg=342.8ms  min=166.95ms med=241.32ms max=1m0s  p(90)=563.96ms p(95)=725.7ms 
     http_reqs......................: 38366  16.808267/s
     iteration_duration.............: avg=1.7s     min=1.22s    med=1.59s    max=1m1s  p(90)=2.03s    p(95)=2.18s   
     iterations.....................: 38366  16.808267/s
     vus............................: 1      min=1   max=50 
     vus_max........................: 50     min=50  max=50 