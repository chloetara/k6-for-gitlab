# Notes for test files (suites) using "scenario"

[[_TOC_]]

## Overal notes

## file "./scenario-per-vu1.js"

### Added / Executed with

1. 2 different URL - for comparing results: 
- https://inclusiva.io;
- https://flugel.it.

**Note:** Here the test file has been executed separately against each endpoint (URL) - for instance, 3 times against the first, and 3 times against the second one.

2. Increased amount of VUs and iterations to `20/30` from `10/20` in the `start-scenario.js`.

3. Added customized metrics **Rate** for failed requests.

4. Added thresholds for timing - `http_req_duration`:
- `['p(95)<900']` - the 95th percentile response time must be below 900 ms.
- `['p(90)<600']` - the 90th percentile response time must be below 600 ms.
In this case, should be fulfilled 2 conditions at once.

### Short conclusions:

1. The endpoint `https://flugel.it` does not meet defined conditions for timing, one of thresholds is always failed despite constant delivery the page (code 200 in 100%). Otherwise, the endpoint `https://inclusiva.io` is always passed these simple thresholds.
2. In this scenario - `per-vu-iterations` we can define exact number of iterations and requests for an endpoint.      
The later gives us more accurate output's results those might be used for comparison and/or improvement. 

## file "./scenario-per-vu2.js" 

### Overall (draft) notes

1. Implemented `group()` - a number of endpoints against which a module is making requests - to the features of the previous file (scenario "per-vu", thresholds, Rate);

2. It's unclear, in the first place, how to correctly use `sleep()` - getting for the first time different results. For now, it is a main goal for improvement. 

3. As it has been mentioned above using **scenario** `pre-vu` gives us possibility to define more accuratly endpoint's capacity for timing.