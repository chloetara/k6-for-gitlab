# Brief overview for Load testing with k6. 

[[_TOC_]]

## Testing tool - k6

### About
1. "k6 is a developer-centric, free and open-source load testing tool built for making performance testing a productive and enjoyable experience." 
<!-- [official documentation's page](https://k6.io/docs/#what-is-k6).  -->

2. [k6 API documentation.](https://k6.io/docs/).

3. [Installation page.](https://k6.io/docs/getting-started/installation/) 


### Running testing scripts 

1. The test file is a JavaScript file with the extention - `.js`; e.g. - `load_test.js` And scripts themselves are written in JS.

2. Doesn't require to have anything else to run by default.  It runs from terminal (bash, CLI, etc.)

> k6 run testFile.js

**Note:** All scripts are running in Linux Ubuntu 18.04 till a necessity to test/run scripts in other enviroment (Windows, another Linux distro).   


## List of testing endpoints (APIs, websites, etc.)  

### Primer

1. Primarily, the target endpoint is - [`https://inclusiva.io`](https://inclusiva.io).

### The list of possible testing endpoints

- https://httpbin.org/ - a variety of testing endpoints;
- https://www.saucedemo.com/ - endpoints those are mocking e-commerce;
- https://hookbin.com/;
- https://requestbin.com/.