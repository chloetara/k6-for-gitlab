'use strict'

import http from 'k6/http';
import { sleep, check } from 'k6';

let mainURL = 'https://inclusiva.io';

// Entry 1
export let options = {
    vus: 30,
    duration: '30s'
};


export default function() {
    let resp = http.get(mainURL);

    check(resp, {
    'Response status is 200': (resp) => resp.status === 200,
    'Response time below 1000ms' : (resp) => resp.timings.duration < 1000,
    'Time to first bite (TTFB) below 500ms' : (resp) => resp.timings.waiting < 500,
    'Time spent blocked/waiting for a free TCP connection slot below 300ms' : (resp) => resp.timings.blocked < 300,
    });
    sleep(1);
};

