'use strict'

import http from 'k6/http';
import { sleep, check } from 'k6';
import { Rate } from 'k6/metrics';

// let mainURL = 'https://inclusiva.io';
// passed threshold [['p(95)<900'], {vus: 10, duration: '10s',} ]; fact: p(95)=893.4ms
let mainURL = 'https://flugel.it';
// passed threshold [['p(95)<900'], {vus: 10, duration: '10s',} ]; fact: p(95)=884.05ms


let failRate = new Rate ('failed_requests');

export let options = {
    vus: 10,
    duration: '10s',
    thresholds: {
      failed_requests: ['rate<=0'],
      http_req_duration: ['p(95)<900'],
    },
};

export default function() {
    let result = http.get(mainURL);

    check(result, {
      'HTTP response code is 200' : (result) => result.status == 200
    });

    failRate.add(result.status !== 200);

    sleep(1);
};
